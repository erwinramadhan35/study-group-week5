import Nav from './Nav'
import AppRoutes from './AppRouter'


function App() {
  return (
    <div className="container">
      <Nav />
      <AppRoutes />
    </div>
  );
}

export default App;
