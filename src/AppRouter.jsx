import Home from './components/Home'
import ListBrand from './components/ListBrand'
import { Switch, Route } from 'react-router-dom'

const AppRouter = () => {
    return (
            <Switch>
                <Route exact path='/'>
                    <Home />
                </Route>
                <Route exact path='/listbrand'>
                    <ListBrand />
                </Route>
            </Switch>
    )
}

export default AppRouter
