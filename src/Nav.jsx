import { Link } from 'react-router-dom'


const Nav = () => {
    return (
        <nav>
            <ul>
                <li><Link to="/">FormSearch</Link></li>
                <li><Link to="/listbrand">List Brand</Link></li>
            </ul>
        </nav>
    )
}

export default Nav
