import { useState } from 'react'
import { Button, Form, FormGroup, Input, Label, Spinner } from "reactstrap";
import { useSelector, useDispatch } from 'react-redux'
import { searchPhone } from '../redux/action'

const Home = () => {

    const [name, setName] = useState("")
    const globalState = useSelector(state => state)
    console.log(globalState);
    const phone = useSelector(state => state.phone)
    console.log(phone);
    const dispatch = useDispatch()

    const handleName = (e) => {
        setName(e.target.value)
    }

    const handleSubmit = (e) => {
        e.preventDefault()
        dispatch(searchPhone(name))
    }

    return (
        <div>
            <div className="row">
                <div className="col-12">
                    <h2>Harga Hand Phone</h2>
                    <Form inline onSubmit={handleSubmit}>
                        <FormGroup>
                            <Label className="mr-2">Name</Label>
                            <Input className="mr-2" type="text" name="name" placeholder="Phone Name" onChange={handleName} />
                        </FormGroup>
                        <Button type="submit">Search</Button>
                    </Form>
                    <ul>
                        {globalState.isLoading && <Spinner color="secondary" />}
                        {!globalState.isLoading && globalState.data.map((phone, i) => <li key={i}>{phone.phone_name}</li>)}
                    </ul>
                </div>
            </div>
        </div>
    )
}

export default Home