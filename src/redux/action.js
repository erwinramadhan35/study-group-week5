import axios from 'axios'

export const types = {
    SEARCH_BRAND: "SEARCH_BRAND",
    SEARCH_SUCCESS: "SEARCH_SUCCESS"
}

export const searchPhone = (name) => (dispatch) => {
    dispatch({ type: types.SEARCH_BRAND })
    const url = `https://api-mobilespecs.azharimm.tk/brands/${name}`
    axios.get(url)
        .then(res => {
            // console.log(res)
            dispatch({ type: types.SEARCH_SUCCESS, phone: res.data.data.phones })
        })
        .catch(err => console.log(err))
}