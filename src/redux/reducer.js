import { types } from './action'

const globalState = {
    phone: "",
    isLoading: false
}

const rootReducer = (state = globalState, action) => {
    switch (action.type) {
        case types.SEARCH_BRAND:
            return {
                ...state,
                isLoading: true
            }
        case types.SEARCH_SUCCESS:
            return {
                ...state,
                phone: action.phone,
                isLoading: false
            }
        default:
            return state
    }
}

export default rootReducer
